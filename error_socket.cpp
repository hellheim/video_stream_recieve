#include <iostream>
#include "header/error_socket.hpp"
#include "header/error_.hpp"


void error_socket(int sockfd, std::string message)
{
    if(sockfd < 0)
        error_(message);
}