#include "header/config_socket.hpp"
#include "header/error_socket.hpp"
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

void config_socket(config_struct *config_, char ** argv)
{
    int n;
    config_->sockfd = socket(AF_INET,SOCK_STREAM,0);
    config_->portno = atoi(argv[3]);

    config_->serv_addr.sin_family = AF_INET;
    config_->serv_addr.sin_addr.s_addr = inet_addr(argv[1]);
    config_->serv_addr.sin_port = htons(config_->portno);

    config_->client_addr.sin_family = AF_INET;
    config_->client_addr.sin_addr.s_addr = inet_addr(argv[2]);
    config_->client_addr.sin_port = htons(config_->portno);

    error_socket(config_->sockfd,"ERROR opening socket.. closing program");
    socklen_t client_len = sizeof(config_->client_addr);
    n = connect(config_->sockfd,(struct sockaddr*)&(config_->client_addr),client_len);
    error_socket(n, "ERROR acception connection.. closing program");
}