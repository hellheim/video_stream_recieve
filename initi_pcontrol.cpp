#include "header/initi_pcontrol.hpp"

void initi_pcontrol(int client_s)
{
    char buffer[12];
    int n;
    bzero(buffer,sizeof(buffer));
    n = read(client_s,buffer,sizeof(buffer));
    if(n < 0)
        error_("can't read from control socket");
    if(strcmp(buffer,SEND_STREAM)==0)
    {
        bzero(buffer,sizeof(buffer));        
        strcpy(buffer, BPID);
        n = write(client_s, buffer, sizeof(buffer));
        if(n < 0)
            error_("can't read from control socket");
    }
    

    std::cout << "sent bpid." << std::endl;
}