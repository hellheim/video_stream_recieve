#include "header/retrieve_frames_r.hpp"

bool retrieve_frames_r(std::vector<cv::Mat> *frames, int sockfd, int nbr_frames)
{
    char buffer[] = SEND_STREAM;
    bool ack = false;
    float duration, bandwidth, average_duration, average_bandwidth;
    std::vector<float> durations, bandwidths;
    //vector of frame
    cv::Mat img = cv::Mat::zeros(HEIGHT , WIDTH, CV_8UC3);
    int imgSize =frame_size(CV_8UC3), n, t1, t2;
    uchar sockData[imgSize];
    protocol(sockfd,buffer,"ERROR can't write to reciever.. closing program");
    bzero(buffer,sizeof(buffer));
    n = read(sockfd,buffer,sizeof(buffer));
    if(strcmp(buffer,ACCEPT_STREAM) == 0)
        ack = true;
    std::cout << "Starting recieving data.." << std::endl;
    while(ack)
    {
        bzero(sockData,sizeof(sockData));
        bzero(buffer,sizeof(buffer));
        t1= cv::getTickCount();
        n = recv(sockfd,sockData, imgSize,MSG_WAITALL);
        error_socket(n,"ERROR failed image reception.. closing program");
        t2 = cv::getTickCount();
        duration = (t2 - t1) / cv::getTickFrequency();
        bandwidth = (imgSize / 1024) / duration;
        durations.push_back(duration);
        bandwidths.push_back(bandwidth);
        img.data = sockData;
        cv::Mat tmp = img.clone();
        frames->push_back(tmp);
        //here detection
        //here recognition

        if(frames->size() < nbr_frames)
        {
            protocol(sockfd,SEND_STREAM,"ERROR writing continuous stream to sender..");
        }
        else
        {
            protocol(sockfd,STOP_STREAM,"ERROR writing continuous stream to sender..");  
        }
        bzero(buffer, sizeof(buffer));
        n = read(sockfd,buffer,sizeof(buffer));
        if(strcmp(buffer,ACCEPT_STREAM) == 0)
            ack = true;
        else
            ack = false;   
        n = 0;
    }
    if(!ack)
    {
        average_duration = std::accumulate(durations.begin(),durations.end(),0.0) / durations.size();
        average_bandwidth = std::accumulate(bandwidths.begin(), bandwidths.end(),0.0)/bandwidths.size();
        std::cout << "Transfer frame time: " << average_duration << "seconds"<<std::endl<< "Bandwidth frame: " << average_bandwidth<<"KBps"<<std::endl;
        return true;
    }
    return false;
}