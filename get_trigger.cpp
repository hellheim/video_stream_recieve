#include "header/get_trigger.hpp"

bool_tr get_trigger(int client_sockfd)
{
    char buffer[12];
    int n;
    bzero(buffer,sizeof(buffer));
    n = read(client_sockfd, buffer, sizeof(buffer));
    if(n < 0)
        error_("can't read from control socket");
    if(strcmp(buffer,SEND_STREAM) == 0)
        return sends;
    else if(strcmp(buffer,EXIT) == 0)
        return stop;
    else
        return notsend;
}