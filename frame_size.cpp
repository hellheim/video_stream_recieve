#include <opencv2/core/mat.hpp>
#include "header/frame_size.hpp"

int frame_size(int type)
{
    cv::Mat img = cv::Mat::zeros(HEIGHT,WIDTH,type);
    int imgSize = img.total()*img.elemSize();
    return imgSize;
}