#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <numeric>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h> 


#define WIDTH 640
#define HEIGHT 480

#include "header/error_.hpp"
#include "header/error_socket.hpp"
#include "header/config_struct.hpp"
#include "header/config_socket.hpp"
#include "header/frame_size.hpp"
#include "header/protocol.hpp"
#include "header/send_frames.hpp"
#include "header/retrieve_frames_r.hpp"
#include "header/config_socket_control.hpp"
#include "header/config_socket_send.hpp"
#include "header/get_trigger.hpp"
#include "header/initi_pcontrol.hpp"
#include "header/sig_handler.hpp"

using namespace std;
using namespace cv;

int fd_sig_handler = 0;

int main(int argc, char** argv)
{
    config_struct config_;
    config_struct_local config_control, config_local;
    bool_tr control_trigger = notsend;
    vector<Mat> frame_stream;
    if(argc < 6)
    {
        error_("Please use like: "+ String(argv[0])+" <local-ip> <client-ip> <port-number> <local-socket> <control-socket>");
    }
    config_socket(&config_,argv);
    config_socket_send(&config_control, argv[5]); //control socket
    initi_pcontrol(config_control.sockfd);
    fd_sig_handler = config_control.sockfd;
    signal(SIGINT,sig_handler);
    config_socket_send(&config_local,argv[4]);
    while(control_trigger == notsend)
    {
        control_trigger = get_trigger(config_control.sockfd);
        if(control_trigger == sends)
        {
            retrieve_frames_r(&frame_stream,config_.sockfd,10);
            int nof = static_cast<int>(frame_stream.size());
            string Nof = std::to_string(nof);
            send_frames(&frame_stream,config_local.sockfd,Nof.c_str());
            cout << "---<>--- end sending" << endl;
            frame_stream.clear();
            control_trigger = notsend;
        }
    }
    cout << "Closing program.." << endl;
    
    return 0;
}