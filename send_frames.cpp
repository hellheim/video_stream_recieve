#include "header/send_frames.hpp"
#include "header/frame_size.hpp"
#include "header/error_.hpp"
#include <vector>
#include <string>
#include <string.h>
#include <stdlib.h>

void send_frames(std::vector<cv::Mat> *frames, int serv_sockfd, const char *nof)
{
    char buffer[12];
    int client_sockfd, n, nframes = std::stoi(nof);
    cv::Mat img;
    int imgSize = frame_size(CV_8UC3), j = 0;
    bool quit = false;
    uchar sockData[imgSize];
    bzero(sockData,sizeof(sockData));
    bzero(buffer,sizeof(buffer));
    strcpy(buffer,nof);
    for(int i = strlen(nof); i < 12; i++)
    {
        if(i != 11)
            buffer[i] = '-';
        else
            buffer[i] = '\0';
    }
    n = send(serv_sockfd,buffer,sizeof(buffer),0);
    if(n < 0)
    {
        error_("Unable to recieve");
    }
    bzero(buffer,sizeof(buffer));
    n = recv(serv_sockfd,buffer,sizeof(buffer),0);
    if(n < 0)
    {
        error_( "Unable to read from socket");
    }
    std::cout << "go -<>-"<< buffer << std::endl;
    if(strcmp(buffer,SEND_STREAM)==0)
    {
        while(!quit)
        {
            bzero(buffer,sizeof(buffer));
            img = frames->at(j).clone();
            img = img.reshape(0,1);
            imgSize = img.total()*img.elemSize();
            n = send(serv_sockfd,img.data,imgSize,0);
            if(n < 0)
                error_("ERROR sending image to target..");
            n = read(serv_sockfd,buffer,sizeof(buffer));
            if(strcmp(buffer,SEND_STREAM) == 0)
            {
                j++;
                std::cout << ">";
            }
            else
            {
                quit = true;
                std::cout << "stop sending.." << buffer <<std::endl;
            }
        }
    }
}
