#include "header/config_socket_control.hpp"

void config_socket_control(config_struct_local *config_, const char* socket_name, int *client_sockfd)
{
    strcpy(config_->socket_name, socket_name);
    config_->sockfd = socket(PF_LOCAL,SOCK_STREAM,0);
    config_->serv_addr.sun_family = AF_LOCAL;
    strcpy(config_->socket_name,socket_name);
    strcpy(config_->serv_addr.sun_path,config_->socket_name);
    socklen_t length_ = sizeof(config_->serv_addr);
    bind(config_->sockfd,(const sockaddr*)&(config_->serv_addr),length_);
    listen(config_->sockfd,1);
    *client_sockfd = accept(config_->sockfd,(struct sockaddr*) &(config_->client_addr),&length_);
    std::cout << "Accepted connection.." << std::endl;
}