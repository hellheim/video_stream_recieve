#ifndef GET_TRIGGER_HPP
#define GET_TRIGGER_HPP

#include <iostream>
#include <string>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include "config_struct.hpp"
#include "error_.hpp"
#include "protocol.hpp"

//function to get a trigger from control process
enum bool_tr {
    sends,
    notsend,
    stop,
    yes
};

bool_tr get_trigger(int client_sockfd);

#endif