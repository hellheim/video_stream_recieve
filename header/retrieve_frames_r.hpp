#ifndef RETRIEVE_FRAMES_R_HPP
#define RETRIEVE_FRAMES_R_HPP

#include <opencv2/opencv.hpp>
#include "config_struct.hpp"
#include "protocol.hpp"
#include "frame_size.hpp"
#include "error_socket.hpp"
#include "config_socket.hpp"
#include <stdio.h>

bool retrieve_frames_r(std::vector<cv::Mat> *frames, int sockfd, int nbr_frames);


#endif