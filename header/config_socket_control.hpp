#ifndef CONFIG_SOCKET_CONTROL_HPP
#define CONFIG_SOCKET_CONTROL_HPP

#include <iostream>
#include <string>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include "config_struct.hpp"


//function to configure control_socket


void config_socket_control(config_struct_local *config_, const char* socket_name, int *client_sockfd);

#endif