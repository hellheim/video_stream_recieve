#ifndef INITI_PCONTROL_HPP
#define INITI_PCONTROL_HPP

#include <iostream>
#include "config_struct.hpp"
#include "error_.hpp"
#include "protocol.hpp"

#define BPID "0"

void initi_pcontrol(int client_s);

#endif